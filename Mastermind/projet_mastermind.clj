(ns theme01-clojure.projet_mastermind
  (:use midje.sweet))


(declare code-secret)
(declare check?)
(declare freqs-dispo)
(declare filtre-indications)
(declare indications)
(declare arret-jeu)
(declare lance-game)
(declare main-game)

(defn code-secret []
  (loop [k 0 , res []]
    (if (< k 4)
      (recur (+ k 1) (conj res (rand-nth [:rouge :bleu :vert :jaune :noir :blanc])))
      res)))

(defn check? [u, v]
  (def indic (indications u v))
  (def filt (filtre-indications u v indic))
  (frequencies filt))

(defn freqs-dispo [u, v]
  (loop [u u, v v, res {}]
    (if (seq u)
      (if (not (= (first v) :noir))
        (if (contains? res (first u))
          (recur (rest u) (rest v) (assoc res (first u) (inc (get res (first u)))))
          (recur (rest u) (rest v) (assoc res (first u) 1)))
        (recur (rest u) (rest v ) (assoc res (first u) 0)))
      res)))

(defn filtre-indications [u, v, w]
  (def my_map (freqs-dispo u w))
  (loop  [u u, v v, w w, f (my_map (first v)), res []]
    (if (seq v)
      (cond
        (= (first w) :blanc) (if (>= f 0)
                               (recur u (rest v) (rest w) (- f 1) (conj res (first w)))
                               (recur u (rest v) (rest w) f (conj res :bad)))
        :else (recur u (rest v) (rest w) f (conj res (first w))))
      res)))


(defn indications [v, u]
  (loop [k 0, s u, t v, res []]
    (if (seq s)
      (if (= (first s) (nth t k))
        (recur (inc k) (rest s) t (conj res :noir))
        (if (some #(= (first s) %) t)
            (recur (inc k) (rest s) t (conj res :blanc))
            (recur (inc k) (rest s) t (conj res :bad))))
      res)))


(defn arret-jeu []
  (System/exit 0))


(defn lance-game[n]
  (let [j1 0, j2 0]
    (loop [m n]
      (if (> m 0)
        (do
          (let [code (code-secret)]
            (def k (main-game code))
            (if (< k 12)
              (if (even? m)
               (inc j2)
               (inc j1))
              (if (even? m)
                (inc j1)
                (inc j2))))
          (recur (- m 1)))
        (do
          (if (> j1 j2)
            (print "J1 a gagné")
            (print "J2 a gagné"))
          (arret-jeu))))))



(defn main-game [code]
    (println "Vous avez 12 tentatives")
    (loop [k 0]
      (if (< k 12)
        (do
          (println "Votre tentative :")
          (flush)
          (let [tent (read-line)]
            (def check (check? code tent))
            (when (= (check :noir) 4)
              (print "Gagné")
              k)
            (recur (inc k))))
        (do
          (print "Perdu")
          k))))


(lance-game 5)
